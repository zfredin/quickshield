# quickshield
A fabricatable framed face shield for medical workers.

(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and displayed for any purpose, but must acknowledge the Quickshield project. Copyright is retained and must be preserved. The work is provided as-is; no warranty is provided, and users accept all liability.

## status
Based on doctor feedback, cleanability, and fabricatability concerns, we've gone all-in with Project Manus on a frameless one-piece folded disposable shield design. You can find out more about this project on their project website [here](https://project-manus.mit.edu/fs).

## archive
Over the weekend of March 21-22, fifteen doctors, RNs, and respiratory specialists at a Boston-area hospital reviewed the designs and provided feedback; this has been added to each design listed later in this document. They also provided feedback for the [frameless prototypes](https://gitlab.cba.mit.edu/alfonso/covid19-fast-kirishield).

Based on this feedback, the CBA Quickshield and the first LL design are the clear winners. Pending a review of neoprene as an acceptable material for hospitals, we'll move forward with the Quickshield design since it's quite a bit faster to fabricate and adjusts to a broader range of head sizes.

----
## tested designs

### CBA Quickshield
This design uses three elements:
- a small 2D cut or 3D printed frame, designed for fabricability and material efficiency
- a cut clear plastic face shield
- a padded band (Neoprene or other)

It's adjustable, comfortable, and uses minimal material for manufacture. Machine-agnostic for construction; we Zund and waterjet, but others could mill and cut by hand if needed (or laser).

![quickshield_f](img/quickshield_f.jpg)

![quickshield_g](img/quickshield_g.jpg)

![quickshield_h](img/quickshield_h.jpg)

![quickshield_parts](img/quickshield_parts.jpg)

Files for the three components are [here](models/quickshield).

**Feedback from doctors (3/21-3/22): good.**
- long shield version [second Quickshield prototype] is too long, hits some people in chest when look down
- [LL version 20190316.1] has better geometry/side coverage and is preferable face shield shape
- #1b [first Quickshield prototype] with the shorter shield is better than longer, but [LL version 20190316.1] face shield length/width is still better.
- but this was the fave of the new ones, some preferred [LL version 20190316.1], some preferred this one, both seem totally workable
- Comfortable
- Secure
- Lightweight
- We love this one, 3 people asked to keep it
- critiques: can we clean neoprene? We don't know, but if so this one seems great
- The prototype model just needs a longer strap of neoprene to fit more people's heads

**time study**

We did a run of 15 face shields and videotaped the process to better understand the cycle time for each step. As a test, we also left the protective film on both sides of the PET-G for removal by the end user; we figured this might improve cleanliness of the process.

![quickshield_time_study](img/quickshield_time_study.jpg)

Results:

|step|time start|time end|time (s)|per piece|
|-|-|-|-|-|
|waterjet setup|0:00:06|0:04:05|0:03:59|0:00:16|
|waterjet cut|0:04:05|0:21:32|0:17:27|0:01:10|
|frame deburr|0:21:32|0:28:10|0:06:38|0:00:27|
|shield setup|0:04:07|0:04:46|0:00:39|0:00:03|
|shield cut|0:04:46|0:07:09|0:02:23|0:00:10|
|shield depanel|0:07:09|0:07:35|0:00:26|0:00:02|
|neoprene setup|0:11:15|0:12:45|0:01:30|0:00:06|
|neoprene cut|0:16:05|0:20:25|0:04:20|0:00:17|
|neoprene depanel|0:20:25|0:23:11|0:02:46|0:00:11|
|strap assemble|0:00:00|0:08:53|0:08:53|0:00:36|
|final assemble|0:09:08|0:13:50|0:04:42|0:00:19|
||||per piece total|0:03:35|

The neoprene cut parameters could use some optimization as the knife didn't always make a clean cut, making assembly and depanelization a bit difficult; those times will likely fall a bit. It is worth trying the ShopBot for the frame cut step; while the cycle time probably won't get better, it could help with parallelization provided sufficient machine operators. Strap/final assemble could also be left to the end user, though this is generally not desirable per our discussions with medical professionals.

Some of the work parallelizes reasonably well; for example, the waterjet/mill operator could conceivably deburr previous runs while the machine is cutting the next batch. With three people we should be able to hit 50 shields/hr.

---
### LL version 3
This version, also designed by [Scale Works](https://www.scaleworkspace.com/), can be 2D cut, and has optional 3D printed 'comfort pieces' that increase contact area in various spots. It still isn't terribly material-efficient and requires the use of a rubber band, but seems to be the design the group is settling around. See [models/LLv3](models/LLv3); I suggest waterjet cutting the frame out of 1/4" HDPE and Zunding the rest out of 0.020" PC for test. Image from Scale Works:

![LL_v3_a](img/LL_v3_a.jpg)

Fabricated and tested:

![LL_v3_b](img/LL_v3_b.jpg)

**Feedback from doctors (3/21-3/22): not optimal.**
- falls down a lot onto nose bc top is heavy
- [LL version 20190316.1] - the first one, is better than this bc lighter

### Prusa RC1
Prusa, the same folks that make our lovely FDM printers, has released a face shield that has 3D printed parts and a cut clear plastic panel. The files, including a sliced Gcode version, are [here](models/PrusaRC1). This design uses a rubber band for securing and adds a bottom support; it's been iterated a few times and looked over by a number of healthcare professionals, so it's worth trying (although it really depends on big printfarms for scaling). Image from Prusa:

![prusarc1_a](img/prusarc1_a.png)

Fabricated and tested:

![prusarc1_b](img/prusarc1_b.jpg)

**Feedback from doctors (3/21-3/22): not optimal.**
- top is insecure and falls down, not good
- moves a lot, not good
- shield is too long
- shield is good distance off face

### LL version 20190316.1
This version uses a 3D printed frame that friction-fits to the face. It's a bit painful to put on but otherwise pretty comfortable, and has optional loops for a rubber band to add tension if needed. See [models/LLv20190316](models/LLv20190316) for fabrication files; the frame was designed by the [Scale Works](https://www.scaleworkspace.com/) team while I made the plastic sheet cut file.

![LL_2019316.1_a](img/LL_v20190316-1_a.jpg)

![LL_2019316.1_b](img/LL_v20190316-1_b.jpg)

After half an hour of wearing, the frame left a somewhat painful red mark on my forehead:

![LL_2019316.1_c](img/LL_v20190316-1_c.jpg)

This was mitigated using a bit of self-adhesive neoprene tape:

![LL_2019316.1_d](img/LL_v20190316-1_d.jpg)

The mask was passed off to Sarah's doctor friend for testing on an overnight shift (3/18-3/19). Concerns are fabrication time (the 3D print takes ~4 hours) and comfort.

**Feedback from doctors (3/21-3/22): great.**
- this is one of 3 faves
- Feels secure
- Best shield geometry of length and width
- Good distance off face
- 4 doctors have asked me to keep this prototype
- only negative was slightly uncomfortable at point where presses above earlobes >4 hours but that's minor

### LL version 2.1
This version can be 3D printed or cut. See [models/LLv2-1](models/LLv2-1) for fabrication files, all designed by [Scale Works](https://www.scaleworkspace.com/). For speed, I chose to waterjet the frame out of 1/4" HDPE, and added a bit of neoprene as before to improve comfort.

![LL_2-1_a](img/LL_v2-1_a.jpg)

![LL_2-1_b](img/LL_v2-1_b.jpg)

The clear sheet cut file for this version came from the Helpful Engineering group; my feeling is that it's a bit short, not offering up enough chin protection. Even with the neoprene pad, I got a decently red forehead mark from this one:

![LL_2-1_c](img/LL_v2-1_c.jpg)

I think the red mark was more due to my lack of rubber band for tensioning; I didn't have any rubber bands so I used a bit of string that was probably too tight. Either way, requiring rubber bands or string isn't great; it's another thing to source/lose/break, and rubber bands in particular don't play nice with hair. A cloth covered hair tie of some kind could work here. Of note: another Slack channel member tested the design for a few hours using 7/16" weatherstripping from the hardware store as a pad and didn't report any discomfort or red mark.

Fabricability-wise, the design cuts nicely using a 2D process but isn't particularly material-efficient.

**Feedback from doctors (3/21-3/22): not optimal.**
- falls down a lot onto nose bc top is heavy
- [LL version 20190316.1] - the first one, is better than this bc lighter
